# Nodo APP

## Para contruir una release de la aplicación

Para poder hacer una release para la web ejecute:

~~~bash
flutter build web --release
~~~

Luego contruyala con Docker:

~~~bash
docker build -t nodo-web:0.0.1 .
~~~
