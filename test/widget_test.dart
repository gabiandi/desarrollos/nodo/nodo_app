import 'package:flutter_test/flutter_test.dart';

import 'package:nodo_app/src/app.dart';

void main() {
  testWidgets('Sample test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(const App());
  });
}
