import 'package:flutter_dotenv/flutter_dotenv.dart';

abstract class AppEnviromentsVariables {
  static String? appBasePath = dotenv.env['APP_BASE_PATH'];
}
