import 'package:flutter/material.dart';
import 'app_bar_actions.dart';
import 'app_bar_icons.dart';
import 'app_bar_search_box.dart';

PreferredSizeWidget appBar() {
  return AppBar(
    toolbarHeight: 110,
    centerTitle: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(20),
        bottomRight: Radius.circular(20),
      ),
    ),
    flexibleSpace: const FlexibleSpaceBar(
      title: Row(
        children: [
          Padding(padding: EdgeInsets.only(left: 32)),
          AppBarIcons(),
          Spacer(),
          AppBarSearchBox(),
          Spacer(),
          AppBarActions(),
          Padding(padding: EdgeInsets.only(right: 32)),
        ],
      ),
    ),
  );
}
