import 'dart:convert';
import 'package:flutter/material.dart';
import '../router/manager.dart';
import '../api/product.dart';

class AppProductsCards extends StatelessWidget {
  const AppProductsCards({super.key});

  Stream<Map<String, dynamic>> getProductsCategories() async* {
    // Se buscan todas las categorias de producto
    final productCategories = await getCategories();

    for (final productCategory in productCategories) {
      // Se obtienen las plantillas de productos por cada categoria obtenida
      final templates = await getProductsForCategory(productCategory['name']);

      // Si la categoria no tiene productos se sigue
      if (templates.isEmpty) continue;

      // Se añaden todas las variantes a cada categoria
      List<dynamic> variants = [];

      for (final template in templates) {
        variants.addAll(template['products']);
      }

      yield {
        productCategory['name'].toString(): variants,
      };
    }
  }

  Widget buildProducts(BuildContext context, dynamic product) {
    final Widget image;

    if (product['images'].length >= 1) {
      image = Image.memory(
        base64Decode(product['images'][0]['image']),
        fit: BoxFit.fitWidth,
      );
    } else {
      image = const Center(
        child: Text(
          'No foto',
          style: TextStyle(fontSize: 24),
        ),
      );
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        child: Container(
          width: 200,
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(4)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey, //New
                blurRadius: 8.0,
                offset: Offset(0, 14),
              ),
            ],
          ),
          child: Column(
            children: [
              Expanded(child: image),
              const Padding(padding: EdgeInsets.only(bottom: 10)),
              Column(
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 14),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      '\$${product['list_price'].toString()}',
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 14),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      product['description'],
                      style: const TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
        onTap: () =>
            AppRouterManager.of(context).goProductPage(product['code']),
      ),
    );
  }

  Widget buildCategory(String category, List<dynamic> templates) {
    final scrollController = ScrollController();

    return Padding(
      padding: const EdgeInsetsDirectional.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.only(bottom: 30, left: 24),
            child: Text(
              category,
              style: const TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(
            height: 300,
            child: Scrollbar(
              thumbVisibility: true,
              controller: scrollController,
              child: ListView.builder(
                controller: scrollController,
                itemCount: templates.length,
                scrollDirection: Axis.horizontal,
                padding: const EdgeInsets.only(bottom: 30),
                itemBuilder: (context, index) =>
                    buildProducts(context, templates[index]),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> dataCategories = [];
    return StreamBuilder(
      stream: getProductsCategories(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Column(
            children: dataCategories,
          );
        }

        dataCategories.add(
          buildCategory(snapshot.data!.keys.first,
              snapshot.data![snapshot.data!.keys.first]),
        );

        return Column(
          children: dataCategories,
        );
      },
    );
  }
}
