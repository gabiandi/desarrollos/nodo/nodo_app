import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/app_shoping_list.dart';
import '../themes/colors.dart';
import '../api/product.dart';
import 'app_button.dart';

class AppProductQuantity extends StatelessWidget {
  final String productCode;
  final String productDescription;
  final String? productImage;

  const AppProductQuantity(this.productCode, this.productDescription,
      {super.key, this.productImage});

  List<DropdownMenuItem> buildDropdownItemsCantidad(double quantity) {
    List<DropdownMenuItem> items = [];

    if (quantity == 0) {
      items.add(
        const DropdownMenuItem(
          value: 0,
          child: Text(
            'Sin stock',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      );
    } else {
      for (var i = 0; i <= quantity; i++) {
        items.add(
          DropdownMenuItem(
            value: i,
            child: Text(
              '$i',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        );
      }
    }

    return items;
  }

  @override
  Widget build(BuildContext context) {
    final appShopingList = Provider.of<AppShopingList>(context);

    return FutureBuilder(
      future: getProductQuantity(productCode),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          double quantitySelection = 0;
          double quantityAvailable = snapshot.data!;

          // Si el carrito contiene elementos del producto actual, se resta a
          // la cantidad disponible
          if (appShopingList.shopingElements.containsKey(productCode)) {
            quantityAvailable = quantityAvailable -
                appShopingList.shopingElements[productCode]!['quantity'];
          }

          // Si la cantidad actual supera la disponible, se deja en 0 la
          // seccionada
          if (appShopingList.quantity <= quantityAvailable) {
            quantitySelection = appShopingList.quantity;
          }

          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  const Text(
                    'Cantidad:',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4),
                  ),
                  Row(
                    children: [
                      DropdownButton(
                        value: quantitySelection,
                        items: buildDropdownItemsCantidad(quantityAvailable),
                        onChanged: (value) {
                          appShopingList.quantity = value;
                        },
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 4),
                      ),
                      Text('($quantityAvailable disponibles)'),
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  // TODO: Boton de añadir al carrito
                  // Se quire hacer una animación para indicar que se añadio al carrito
                  AppButton(
                    width: 200,
                    height: 60,
                    function: quantityAvailable > 0
                        ? () {
                            appShopingList.appendProductToShopingList(
                                productCode,
                                productDescription,
                                productImage,
                                quantitySelection);
                          }
                        : null,
                    child: const Text(
                      'Añadir al carrito',
                      style: TextStyle(
                        fontSize: 20,
                        color: AppColor.white,
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4),
                  ),
                  // TODO: Boton de comprar sin añadir al carrito
                  AppButton(
                    width: 200,
                    height: 60,
                    color: AppColor.secundary,
                    function: () {},
                    child: const Text(
                      'Comprar',
                      style: TextStyle(
                        fontSize: 20,
                        color: AppColor.white,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          );
        } else {
          return const Column(
            children: [
              Padding(padding: EdgeInsets.only(top: 8)),
              Row(
                children: [
                  CircularProgressIndicator(),
                  Padding(padding: EdgeInsets.all(4)),
                  Text('Cargando stock...'),
                ],
              ),
            ],
          );
        }
      },
    );
  }
}
