import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:flutter_svg/svg.dart';
import '../../src/router/manager.dart';

class AppBarIcons extends StatelessWidget {
  const AppBarIcons({super.key});

  @override
  Widget build(BuildContext context) {
    final appRouterManager = AppRouterManager.of(context);
    return Row(
      children: [
        InkWell(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: Image.asset(
              'assets/images/logo.jpg',
              width: 80,
            ),
          ),
          onTap: () {
            appRouterManager.pushHomePage();
          },
        ),
        const Padding(padding: EdgeInsets.all(2)),
        InkWell(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: SvgPicture.asset(
              'assets/images/facebook.svg',
              width: 40,
            ),
          ),
          onTap: () async {
            launchUrlString(
              'https://www.facebook.com/Nodo-Brote-Nativo-105907861269315/',
            );
          },
        ),
        const Padding(padding: EdgeInsets.all(2)),
        InkWell(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: SvgPicture.asset(
              'assets/images/instagram.svg',
              width: 40,
            ),
          ),
          onTap: () async {
            launchUrlString(
              'https://www.instagram.com/nodo_brote_nativo/',
            );
          },
        ),
      ],
    );
  }
}
