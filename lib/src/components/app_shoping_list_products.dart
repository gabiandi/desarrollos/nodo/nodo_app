import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/app_shoping_list.dart';
import '../themes/colors.dart';
import '../api/product.dart';
import 'app_button.dart';
import 'app_shoping_list_product_element.dart';

class AppShopingListProducts extends StatelessWidget {
  const AppShopingListProducts({super.key});

  Future<List<Widget>> buildShopingListProducts(
      Map<String, dynamic> shopingListProducts) async {
    List<Widget> productList = [];

    for (final shopingListProduct in shopingListProducts.entries) {
      final productQuantityMax =
          await getProductQuantity(shopingListProduct.key);
      productList.add(AppShopingListProductElement(
        shopingListProduct.key,
        shopingListProduct.value['description'],
        shopingListProduct.value['quantity'],
        productQuantityMax,
        productImage: shopingListProduct.value['image'],
      ));
      productList
          .add(const Padding(padding: EdgeInsets.symmetric(vertical: 10)));
    }

    return productList;
  }

  @override
  Widget build(BuildContext context) {
    final appShopingList = Provider.of<AppShopingList>(context);

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 200),
      child: Container(
        width: double.infinity,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey, //New
              blurRadius: 8.0,
              offset: Offset(0, 14),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(
                vertical: 20,
                horizontal: 40,
              ),
              child: Text(
                'Carrito',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 34,
                ),
              ),
            ),
            FutureBuilder(
              future: buildShopingListProducts(appShopingList.shopingElements),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const Column(
                    children: [
                      Padding(padding: EdgeInsets.only(top: 8)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircularProgressIndicator(),
                          Padding(padding: EdgeInsets.all(4)),
                          Text('Cargando carrito...'),
                        ],
                      ),
                    ],
                  );
                }
                return Column(children: snapshot.data!);
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                AppButton(
                  width: 200,
                  height: 60,
                  function: () {},
                  child: const Text(
                    'Vaciar carrito',
                    style: TextStyle(
                      fontSize: 20,
                      color: AppColor.white,
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 4),
                ),
                AppButton(
                  width: 200,
                  height: 60,
                  color: AppColor.secundary,
                  function: () {},
                  child: const Text(
                    'Comprar',
                    style: TextStyle(
                      fontSize: 20,
                      color: AppColor.white,
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                ),
              ],
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
            ),
          ],
        ),
      ),
    );
  }
}
