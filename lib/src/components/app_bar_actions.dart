import 'package:flutter/material.dart';
import 'package:badges/badges.dart' as badges;
import 'package:provider/provider.dart';
import 'app_bar_actions_item.dart';
import '../providers/app_shoping_list.dart';
import '../router/manager.dart';

class AppBarActions extends StatelessWidget {
  const AppBarActions({super.key});

  @override
  Widget build(BuildContext context) {
    final appShopingList = Provider.of<AppShopingList>(context);

    return Row(
      children: [
        if (appShopingList.shopingElements.isNotEmpty)
          badges.Badge(
            position: badges.BadgePosition.topEnd(top: 18, end: 24),
            badgeContent: Text('${appShopingList.shopingElements.length}'),
            child: AppBarActionsItem(
              Icons.shopping_bag_rounded,
              callback: () => AppRouterManager.of(context).goShopingListPage(),
            ),
          ),
        const Padding(padding: EdgeInsets.symmetric(horizontal: 2)),
        AppBarActionsItem(
          Icons.help,
          callback: () => AppRouterManager.of(context).goInfoPage(),
        ),
      ],
    );
  }
}
