import 'package:flutter/material.dart';
import '../themes/colors.dart';

class AppButton extends StatelessWidget {
  final Widget child;
  final void Function()? function;
  final double width;
  final double height;
  final MaterialColor boxShadowColor;
  final BorderRadius borderRadius;
  final double blurRadius;
  final Color? color;

  const AppButton({
    super.key,
    required this.child,
    this.function,
    this.width = 200,
    this.height = 60,
    this.boxShadowColor = Colors.grey,
    this.borderRadius = const BorderRadius.all(Radius.circular(16)),
    this.blurRadius = 4,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: function != null ? color ?? AppColor.primary : Colors.grey,
        borderRadius: borderRadius,
        boxShadow: [
          BoxShadow(
            color: boxShadowColor,
            blurRadius: blurRadius,
            offset: const Offset(0, 8),
          ),
        ],
      ),
      child: MaterialButton(
        onPressed: function,
        minWidth: double.infinity,
        height: double.infinity,
        child: child,
      ),
    );
  }
}
