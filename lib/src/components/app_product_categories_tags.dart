import 'package:flutter/material.dart';
import '../themes/colors.dart';

class AppProductCategoriesTags extends StatelessWidget {
  final List<dynamic> categories;
  const AppProductCategoriesTags(this.categories, {super.key});

  @override
  Widget build(BuildContext context) {
    List<Widget> productCategories = [];
    for (final category in categories) {
      String label = category['name'].toString();
      productCategories.add(
        Chip(
          labelPadding: const EdgeInsets.all(2.0),
          avatar: CircleAvatar(
            backgroundColor: Colors.white70,
            child: Text(label[0].toUpperCase()),
          ),
          label: Text(
            label,
            style: const TextStyle(color: Colors.white),
          ),
          backgroundColor: AppColor.primary,
          padding: const EdgeInsets.all(8.0),
        ),
      );
    }
    return Row(children: productCategories);
  }
}
