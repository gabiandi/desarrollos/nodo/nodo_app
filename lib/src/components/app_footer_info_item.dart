import 'package:flutter/material.dart';

class AppFooterInfoItem extends StatelessWidget {
  final Icon image;
  final String title;
  final String description;

  const AppFooterInfoItem({
    super.key,
    required this.image,
    required this.title,
    required this.description,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        image,
        Text(
          title,
          style: const TextStyle(fontSize: 28),
        ),
        const Padding(padding: EdgeInsets.symmetric(vertical: 4)),
        SizedBox(
          width: 180,
          child: Text(
            description,
            style: const TextStyle(fontSize: 12),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
