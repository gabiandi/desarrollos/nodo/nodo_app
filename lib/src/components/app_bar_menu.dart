import 'package:flutter/material.dart';
import 'package:nodo_app/src/components/app_bar_menu_item.dart';

class AppBarMenu extends StatelessWidget {
  const AppBarMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: getMenuItems(),
    );
  }

  List<Widget> getMenuItems() {
    return [
      AppBarMenuItem(
        'Página principal',
        bold: true,
        callback: () {},
      ),
      AppBarMenuItem(
        'Frutas',
        callback: () {},
      ),
      AppBarMenuItem(
        'Verduras',
        callback: () {},
      ),
      AppBarMenuItem(
        'Hortalizas',
        callback: () {},
      ),
    ];
  }
}
