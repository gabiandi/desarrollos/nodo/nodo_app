import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:nodo_app/src/components/app_product_categories_tags.dart';
import 'package:nodo_app/src/components/app_product_quantity.dart';

class AppProductDescriptionCard extends StatelessWidget {
  final dynamic productInfo;
  const AppProductDescriptionCard(this.productInfo, {super.key});

  @override
  Widget build(BuildContext context) {
    final Widget image;

    if (productInfo['images'].length >= 1) {
      image = Image.memory(
        base64Decode(productInfo['images'][0]['image']),
        fit: BoxFit.fitWidth,
        width: 400,
        height: 400,
      );
    } else {
      image = const Center(
        child: Text(
          'No foto',
          style: TextStyle(fontSize: 24),
        ),
      );
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 200),
      child: Container(
        width: double.infinity,
        height: 600,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey, //New
              blurRadius: 8.0,
              offset: Offset(0, 14),
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                child: image),
            const SizedBox(
              width: 60,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  productInfo['description'],
                  style: const TextStyle(
                    fontSize: 32,
                  ),
                ),
                Text(
                  '\$${productInfo['list_price']}',
                  style: const TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  'Code: ${productInfo['code']}',
                ),
                AppProductCategoriesTags(productInfo['categories']),
                AppProductQuantity(
                  productInfo['code'],
                  productInfo['description'],
                  productImage: productInfo['images'].length >= 1
                      ? productInfo['images'][0]['image']
                      : null,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
