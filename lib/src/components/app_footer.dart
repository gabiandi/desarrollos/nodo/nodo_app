import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:url_launcher/url_launcher_string.dart';
import '../components/app_footer_info_item.dart';
import '../themes/colors.dart';

class AppFooter extends StatelessWidget {
  const AppFooter({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 360,
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppFooterInfoItem(
                image: Icon(
                  Icons.payment,
                  size: 180,
                  color: AppColor.primary,
                ),
                title: 'Formas de pago',
                description:
                    'Podes pagar con débito, transferencia o mercadopago.',
              ),
              Padding(padding: EdgeInsets.symmetric(horizontal: 40)),
              AppFooterInfoItem(
                image: Icon(
                  Icons.eco_outlined,
                  size: 180,
                  color: AppColor.primary,
                ),
                title: '100% agroecológico',
                description:
                    'Todos los productos que brindamos son libre de agrotóxicos y 100% orgánicos.',
              ),
              Padding(padding: EdgeInsets.symmetric(horizontal: 40)),
              AppFooterInfoItem(
                image: Icon(
                  Icons.safety_divider_outlined,
                  size: 180,
                  color: AppColor.primary,
                ),
                title: 'Puntos de retiro',
                description: 'Las entregas se realizan semanalmente.',
              ),
            ],
          ),
          const Padding(padding: EdgeInsets.symmetric(vertical: 20)),
          Row(
            children: [
              const Padding(padding: EdgeInsets.only(left: 20)),
              const Text(
                'Esta aplicación fue desarrollada por Gabriel Andrés Aguirre',
              ),
              const Padding(padding: EdgeInsets.symmetric(horizontal: 4)),
              InkWell(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: SvgPicture.asset(
                    'assets/images/linkedin.svg',
                    width: 30,
                  ),
                ),
                onTap: () async {
                  launchUrlString(
                    'https://www.linkedin.com/in/gabiandi/',
                  );
                },
              ),
              const Padding(padding: EdgeInsets.symmetric(horizontal: 2)),
              InkWell(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: SvgPicture.asset(
                    'assets/images/gmail.svg',
                    width: 30,
                  ),
                ),
                onTap: () async {
                  launchUrlString(
                    'mailto:gabiandiagui@gmail.com',
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
