import 'package:flutter/material.dart';
import 'package:nodo_app/src/themes/colors.dart';

class AppBarMenuItem extends StatelessWidget {
  final String text;
  final bool bold;
  final void Function()? callback;

  const AppBarMenuItem(
    this.text, {
    this.bold = false,
    this.callback,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: callback,
      child: Text(
        text,
        style: TextStyle(
          color: AppColor.white,
          fontWeight: bold ? FontWeight.bold : FontWeight.normal,
        ),
      ),
    );
  }
}
