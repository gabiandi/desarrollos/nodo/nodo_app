import 'package:flutter/material.dart';
import '../themes/colors.dart';
import 'app_bar_menu.dart';

class AppBarSearchBox extends StatelessWidget {
  const AppBarSearchBox({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Spacer(),
        Container(
          width: 600,
          decoration: BoxDecoration(
            color: AppColor.white,
            borderRadius: BorderRadius.circular(12),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 6),
            child: TextFormField(
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.search,
              autocorrect: false,
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                border: InputBorder.none,
                suffixIcon: IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.search,
                    color: AppColor.primary,
                  ),
                ),
                hintText: 'Buscar productos...',
              ),
            ),
          ),
        ),
        const SizedBox(height: 8),
        const AppBarMenu(),
        const Spacer(),
      ],
    );
  }
}
