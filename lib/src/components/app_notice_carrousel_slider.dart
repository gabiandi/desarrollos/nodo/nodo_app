import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:url_launcher/url_launcher_string.dart';
import '../themes/colors.dart';
import '../providers/app_carousel_notice.dart';
import '../api/news.dart';

class AppNoticeCarouselSlider extends StatelessWidget {
  final appCarouselController = CarouselController();
  final Future<List<Widget>> news = getNews_();

  AppNoticeCarouselSlider({super.key});

  static Future<List<Widget>> getNews_() async {
    List<dynamic> news = await getNews();
    List<Widget> newsWidgets = [];

    for (dynamic new_ in news) {
      newsWidgets.add(buildNew(new_['title'], new_['image'], new_['url']));
    }

    return newsWidgets;
  }

  static Widget buildNew(String title, String image, String url) {
    return Column(
      children: [
        Text(
          title,
          style: const TextStyle(fontSize: 18),
        ),
        const SizedBox(
          height: 14,
        ),
        Expanded(
          child: InkWell(
            child: Image.memory(
              base64Decode(image),
              fit: BoxFit.fitWidth,
            ),
            onTap: () => launchUrlString(url),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final appCarouselNoticeInfo = Provider.of<AppCarouselNoticeInfo>(context);

    return FutureBuilder(
      future: news,
      initialData: const [],
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Column(
            children: [
              CarouselSlider.builder(
                carouselController: appCarouselController,
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index, realIndex) {
                  if (snapshot.data!.isEmpty) {
                    return const Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(),
                        Padding(padding: EdgeInsets.all(4)),
                        Text('Cargando noticias...'),
                      ],
                    );
                  }
                  return snapshot.data![index];
                },
                options: CarouselOptions(
                  height: 400,
                  autoPlay: true,
                  autoPlayInterval: const Duration(seconds: 6),
                  autoPlayAnimationDuration: const Duration(seconds: 1),
                  onPageChanged: (index, reason) {
                    appCarouselNoticeInfo.index = index;
                  },
                ),
              ),
              const SizedBox(height: 10),
              AnimatedSmoothIndicator(
                activeIndex: appCarouselNoticeInfo.index,
                effect: const WormEffect(
                  activeDotColor: AppColor.primary,
                ),
                count: snapshot.data!.length,
                duration: const Duration(milliseconds: 600),
                onDotClicked: (index) {
                  appCarouselController.animateToPage(index);
                },
              )
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }
}
