import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../components/app_button.dart';
import '../providers/app_shoping_list.dart';
import '../themes/colors.dart';

class AppShopingListProductElement extends StatelessWidget {
  final String productCode;
  final String productDescription;
  final String? productImage;
  final double productQuantity;
  final double productQuantityMax;
  final double imageSize;

  const AppShopingListProductElement(this.productCode, this.productDescription,
      this.productQuantity, this.productQuantityMax,
      {super.key, this.imageSize = 120, this.productImage});

  @override
  Widget build(BuildContext context) {
    final appShopingList = Provider.of<AppShopingList>(context);
    final Widget image;

    if (productImage != null) {
      image = Image.memory(
        base64Decode(productImage!),
        fit: BoxFit.fitWidth,
        width: imageSize,
        height: imageSize,
      );
    } else {
      image = Container(
        color: AppColor.white,
        width: imageSize,
        height: imageSize,
        child: const Center(
          child: Text(
            'No foto',
            style: TextStyle(fontSize: 24),
          ),
        ),
      );
    }

    // if (productQuantity > productQuantityMax) {
    //   productQuantity = productQuantityMax;
    // }

    return Row(
      children: [
        const Padding(
          padding: EdgeInsets.only(left: 30, right: 30),
          child: Icon(Icons.close),
        ),
        image,
        Padding(
          padding: const EdgeInsets.only(left: 40, right: 100),
          child: Text(
            productDescription,
            style: const TextStyle(fontSize: 24),
          ),
        ),
        Row(
          children: [
            AppButton(
              width: 40,
              height: 40,
              borderRadius: const BorderRadius.all(Radius.circular(4)),
              blurRadius: 8,
              color: productQuantity <= 0 ? Colors.grey : AppColor.primary,
              child: const Icon(Icons.remove),
              // TODO: Eliminar parpadeo
              // TODO: Quitar accion si se supero el limite
              function: () {
                appShopingList.removeProductToShopingList(productCode, 1);
              },
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: Text(
                '$productQuantity',
                style: const TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            AppButton(
              width: 40,
              height: 40,
              borderRadius: const BorderRadius.all(Radius.circular(4)),
              blurRadius: 8,
              color: productQuantity >= productQuantityMax
                  ? Colors.grey
                  : AppColor.primary,
              child: const Icon(Icons.add),
              // TODO: Eliminar parpadeo
              // TODO: Quitar accion si se supero el limite
              function: () {
                appShopingList.appendProductToShopingList(
                    productCode, productDescription, productImage, 1);
              },
            ),
          ],
        ),
      ],
    );
  }
}
