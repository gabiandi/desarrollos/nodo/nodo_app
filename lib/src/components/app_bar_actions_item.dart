import 'package:flutter/material.dart';
import '../themes/colors.dart';

class AppBarActionsItem extends StatelessWidget {
  final IconData icon;
  final double size;
  final void Function()? callback;

  const AppBarActionsItem(
    this.icon, {
    this.size = 24,
    this.callback,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40,
      height: 40,
      decoration: BoxDecoration(
        color: AppColor.white,
        borderRadius: BorderRadius.circular(8),
      ),
      child: InkWell(
        onTap: callback,
        child: Icon(
          icon,
          color: AppColor.primary,
          size: size,
        ),
      ),
    );
  }
}
