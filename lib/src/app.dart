import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'providers/app_carousel_notice.dart';
import 'providers/app_shoping_list.dart';
import 'router/delegate.dart';
import 'router/parser.dart';
import 'themes/themes.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ListenableProvider(create: (_) => AppCarouselNoticeInfo()),
        ListenableProvider(create: (_) => AppShopingList()),
      ],
      child: MaterialApp.router(
        title: "Nodo",
        routerDelegate: AppRouterDelegate(),
        routeInformationParser: AppRouteInformationParser(),
        theme: AppTheme.defaultTheme,
      ),
    );
  }
}
