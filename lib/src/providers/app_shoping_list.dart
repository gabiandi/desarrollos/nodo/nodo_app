import 'package:flutter/material.dart';

class AppShopingList with ChangeNotifier {
  final Map<String, Map<String, dynamic>> _shopingElements = {};
  Map<String, Map<String, dynamic>> get shopingElements => _shopingElements;

  double _quantity = 0;
  double get quantity => _quantity;
  set quantity(double newQuantity) {
    _quantity = newQuantity;
    notifyListeners();
  }

  void appendProductToShopingList(
      String productCode, String description, String? image, double quantity) {
    if (quantity == 0) {
      return;
    } else if (_shopingElements.containsKey(productCode)) {
      _shopingElements[productCode]!['quantity'] =
          _shopingElements[productCode]!['quantity'] + quantity;
    } else {
      _shopingElements[productCode] = {};
      _shopingElements[productCode]!['description'] = description;
      _shopingElements[productCode]!['image'] = image;
      _shopingElements[productCode]!['quantity'] = quantity;
    }
    notifyListeners();
  }

  void removeProductToShopingList(String productCode, int quantity) {
    if (quantity == 0 || _shopingElements[productCode] == null) {
      return;
    } else if (_shopingElements[productCode]!['quantity'] - quantity < 0) {
      return;
    } else {
      _shopingElements[productCode]!['quantity'] =
          _shopingElements[productCode]!['quantity'] - quantity;
    }
    notifyListeners();
  }

  void deleteProductToShopingList(String productCode) {
    if (_shopingElements[productCode] == null) {
      return;
    } else {
      _shopingElements.remove(productCode);
    }
    notifyListeners();
  }

  void clearShopingList() {
    _shopingElements.clear();
    notifyListeners();
  }
}
