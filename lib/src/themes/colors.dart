import 'package:flutter/material.dart';

class AppColor {
  static const background = Color(0xFFE6E6E6);
  static const white = Color.fromARGB(255, 240, 240, 240);
  static const black = Color.fromARGB(255, 0, 0, 0);
  static const primary = Color(0xFF3E754D);
  static const secundary = Color(0xFF2D4E36);
}
