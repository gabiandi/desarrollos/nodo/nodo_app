import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'colors.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class NoTransitionsBuilder extends PageTransitionsBuilder {
  const NoTransitionsBuilder();

  @override
  Widget buildTransitions<T>(
    PageRoute<T>? route,
    BuildContext? context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget? child,
  ) {
    return child!;
  }
}

class AppTheme {
  static final defaultTheme = ThemeData(
    pageTransitionsTheme: PageTransitionsTheme(
      builders: kIsWeb
          ? {
              for (final platform in TargetPlatform.values)
                platform: const NoTransitionsBuilder(),
            }
          : const {
              // handel other platforms you are targeting
            },
    ),
    brightness: Brightness.light,
    appBarTheme: const AppBarTheme(
      backgroundColor: AppColor.primary,
      shadowColor: Colors.black,
      surfaceTintColor: Colors.transparent,
    ),
    colorScheme: ThemeData.light().colorScheme.copyWith(
          background: AppColor.white,
          primary: AppColor.primary,
          secondary: AppColor.secundary,
        ),
    scaffoldBackgroundColor: AppColor.background,
    textTheme: GoogleFonts.interTextTheme(),
    useMaterial3: true,
  );
}
