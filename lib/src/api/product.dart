import 'dart:convert';
import 'package:http/http.dart' as http;
import '../env.dart';

Future<List<dynamic>> getProductInfo(String productCode) async {
  final response = await http.get(
    Uri.parse(
        '${AppEnviromentsVariables.appBasePath}/product_variants?filters=code::eq:$productCode'),
  );

  if (response.statusCode != 200) {
    return [];
  }

  return jsonDecode(response.body);
}

Future<double> getProductQuantity(String productCode,
    {String warehouse = 'STO'}) async {
  final response = await http.get(
    Uri.parse(
        '${AppEnviromentsVariables.appBasePath}/product_by_location?warehouse=$warehouse&product=$productCode'),
  );

  final dynamic result = jsonDecode(response.body);

  return result.isEmpty ? 0.0 : result['quantity'];
}

Future<List<dynamic>> getProductsForCategory(String category) async {
  final response = await http.get(
    Uri.parse(
        '${AppEnviromentsVariables.appBasePath}/product_templates?filters=categories.name::eq:$category'),
  );
  return jsonDecode(response.body);
}

Future<List<dynamic>> getCategories() async {
  final response = await http.get(
    Uri.parse('${AppEnviromentsVariables.appBasePath}/product_categories'),
  );
  return jsonDecode(response.body);
}
