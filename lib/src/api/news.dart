import 'dart:convert';
import 'package:http/http.dart' as http;
import '../env.dart';

Future<List<dynamic>> getNews() async {
  final response = await http.get(
    Uri.parse('${AppEnviromentsVariables.appBasePath}/news'),
  );

  final List<dynamic> noticias = jsonDecode(response.body);
  final List<dynamic> news = [];

  for (final noticia in noticias) {
    news.add({
      'title': noticia['title'],
      'image': noticia['image'],
      'url': noticia['url'],
    });
  }

  return news;
}
