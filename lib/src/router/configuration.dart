import 'location.dart';

class AppRouterConfiguration {
  final String location;

  AppRouterConfiguration.home() : location = AppRouterLocation.home;
  AppRouterConfiguration.unknown() : location = AppRouterLocation.unknown;
  AppRouterConfiguration.product(String? productCode)
      : location = AppRouterLocation.product(productCode);
  AppRouterConfiguration.shopingList()
      : location = AppRouterLocation.shopingList;
  AppRouterConfiguration.info() : location = AppRouterLocation.info;
}
