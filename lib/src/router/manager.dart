import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'configuration.dart';
import 'location.dart';
import '../pages/home.dart';
import '../pages/unknown.dart';
import '../pages/product.dart';
import '../pages/shoping_list.dart';
import '../pages/info.dart';

class AppRouterManager extends ChangeNotifier {
  // TODO: cambiar animacion entre paginas
  static AppRouterManager of(BuildContext context) {
    return Provider.of<AppRouterManager>(context, listen: false);
  }

  final List<Page> _pages = [createHomePage()];

  List<Page> get pages => List.unmodifiable(_pages);

  static AppRouterConfiguration getRouterConfiguration(String? location) {
    final uri = Uri.parse(location ?? '/404');

    if (uri.path == AppRouterLocation.home) {
      return AppRouterConfiguration.home();
    } else if (uri.path == AppRouterLocation.product(uri.pathSegments.last)) {
      return AppRouterConfiguration.product(uri.pathSegments.last);
    } else if (uri.path == AppRouterLocation.shopingList) {
      return AppRouterConfiguration.shopingList();
    } else if (uri.path == AppRouterLocation.info) {
      return AppRouterConfiguration.info();
    }
    return AppRouterConfiguration.unknown();
  }

  static String getLocation(AppRouterConfiguration configuration) {
    final uri = Uri.parse(configuration.location);

    if (uri.toString() == AppRouterLocation.home) {
      return AppRouterLocation.home;
    } else if (uri.toString() ==
        AppRouterLocation.product(uri.pathSegments.last)) {
      return AppRouterLocation.product(uri.pathSegments.last);
    } else if (uri.toString() == AppRouterLocation.shopingList) {
      return AppRouterLocation.shopingList;
    } else if (uri.toString() == AppRouterLocation.info) {
      return AppRouterLocation.info;
    }
    return AppRouterLocation.unknown;
  }

  static MaterialPage createHomePage() {
    return MaterialPage(
      child: const HomePage(),
      key: UniqueKey(),
      name: AppRouterLocation.home,
    );
  }

  static MaterialPage createUnknownPage() {
    return MaterialPage(
      child: const UnknownPage(),
      key: UniqueKey(),
      name: AppRouterLocation.unknown,
    );
  }

  static MaterialPage createProductPage(String productCode) {
    return MaterialPage(
      child: ProductPage(productCode: productCode),
      key: UniqueKey(),
      name: AppRouterLocation.product(productCode),
    );
  }

  static MaterialPage createShopingListPage() {
    return MaterialPage(
      child: const ShopingListPage(),
      key: UniqueKey(),
      name: AppRouterLocation.shopingList,
    );
  }

  static MaterialPage createInfoPage() {
    return MaterialPage(
      child: const InfoPage(),
      key: UniqueKey(),
      name: AppRouterLocation.info,
    );
  }

  bool didPop(RouteSettings settings) {
    final result = _pages.remove(settings);
    notifyListeners();
    return result;
  }

  void goHomePage() {
    if ((_pages.length == 1) && (_pages.last.name == AppRouterLocation.home)) {
      return;
    }
    _pages.removeRange(1, _pages.length);
    notifyListeners();
  }

  void pushHomePage() {
    if ((_pages.length == 1) && (_pages.last.name == AppRouterLocation.home)) {
      return;
    }
    _pages.add(createHomePage());
    notifyListeners();
  }

  void goUnknownPage() {
    if (_pages.last.name == AppRouterLocation.unknown) {
      return;
    }
    _pages.add(createUnknownPage());
    notifyListeners();
  }

  void goProductPage(String productCode) {
    if (_pages.last.name == AppRouterLocation.product(productCode)) {
      return;
    }
    _pages.add(createProductPage(productCode));
    notifyListeners();
  }

  void goShopingListPage() {
    if (_pages.last.name == AppRouterLocation.shopingList) {
      return;
    }
    _pages.add(createShopingListPage());
    notifyListeners();
  }

  void goInfoPage() {
    if (_pages.last.name == AppRouterLocation.info) {
      return;
    }
    _pages.add(createInfoPage());
    notifyListeners();
  }
}
