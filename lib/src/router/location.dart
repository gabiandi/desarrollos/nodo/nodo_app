class AppRouterLocation {
  static const String home = '/';
  static const String unknown = '/404';
  static String product(String? productCode) => '/product/$productCode';
  static const String shopingList = '/shoping_list';
  static const String info = '/info';
}
