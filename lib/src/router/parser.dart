import 'package:flutter/material.dart';
import 'manager.dart';
import 'configuration.dart';

class AppRouteInformationParser
    extends RouteInformationParser<AppRouterConfiguration> {
  @override
  Future<AppRouterConfiguration> parseRouteInformation(
      RouteInformation routeInformation) async {
    return AppRouterManager.getRouterConfiguration(routeInformation.uri.path);
  }

  @override
  RouteInformation? restoreRouteInformation(
      AppRouterConfiguration configuration) {
    return RouteInformation(
      uri: Uri.parse(AppRouterManager.getLocation(configuration)),
    );
  }
}
