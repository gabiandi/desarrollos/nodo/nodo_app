import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'configuration.dart';
import 'manager.dart';
import 'location.dart';

class AppRouterDelegate extends RouterDelegate<AppRouterConfiguration>
    with
        ChangeNotifier,
        PopNavigatorRouterDelegateMixin<AppRouterConfiguration> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  final _routerManager = AppRouterManager();

  AppRouterDelegate() {
    _routerManager.addListener(notifyListeners);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _routerManager,
      child: Consumer<AppRouterManager>(
        builder: (context, value, child) {
          return Navigator(
            pages: _routerManager.pages,
            onPopPage: (route, result) {
              bool didPop = route.didPop(result);
              if (!didPop) {
                return false;
              }
              return _routerManager.didPop(route.settings);
            },
          );
        },
      ),
    );
  }

  @override
  GlobalKey<NavigatorState>? get navigatorKey => _navigatorKey;

  @override
  AppRouterConfiguration? get currentConfiguration {
    return AppRouterManager.getRouterConfiguration(
        _routerManager.pages.last.name);
  }

  @override
  Future<void> setNewRoutePath(AppRouterConfiguration configuration) async {
    final uri = Uri.parse(configuration.location);

    if (uri.toString() == AppRouterLocation.home) {
      _routerManager.goHomePage();
      return;
    } else if (uri.toString() ==
        AppRouterLocation.product(uri.pathSegments.last)) {
      _routerManager.goProductPage(uri.pathSegments.last);
      return;
    } else if (uri.toString() == AppRouterLocation.shopingList) {
      _routerManager.goShopingListPage();
      return;
    } else if (uri.toString() == AppRouterLocation.info) {
      _routerManager.goInfoPage();
      return;
    }
    _routerManager.goUnknownPage();
  }
}
