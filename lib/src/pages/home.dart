import 'package:flutter/material.dart';
import '../components/app_notice_carrousel_slider.dart';
import '../components/app_products_cards.dart';
import '../components/app_bar.dart';
import '../components/app_footer.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            const Padding(padding: EdgeInsets.symmetric(vertical: 10)),
            AppNoticeCarouselSlider(),
            const Padding(padding: EdgeInsets.symmetric(vertical: 10)),
            const AppProductsCards(),
            const Padding(padding: EdgeInsets.symmetric(vertical: 10)),
            const AppFooter(),
          ],
        ),
      ),
    );
  }
}
