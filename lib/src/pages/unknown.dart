import 'package:flutter/material.dart';
import '../components/app_bar.dart';
import '../components/app_footer.dart';

class UnknownPage extends StatelessWidget {
  const UnknownPage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: cambiar back button
    return Scaffold(
      appBar: appBar(),
      body: const Column(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "404",
                  style: TextStyle(
                    fontSize: 100,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "Lo sentimos, esta página no existe...",
                  style: TextStyle(fontSize: 32),
                ),
              ],
            ),
          ),
          AppFooter(),
        ],
      ),
    );
  }
}
