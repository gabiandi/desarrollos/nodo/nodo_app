import 'package:flutter/material.dart';
import '../components/app_bar.dart';
import '../components/app_footer.dart';
import '../components/app_product_description_card.dart';
import '../api/product.dart';

class ProductPage extends StatelessWidget {
  final String productCode;

  const ProductPage({super.key, required this.productCode});

  Widget createErrorProductPage() {
    return const Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Lo sentimos, pero ocurrio un error inesperado...',
            style: TextStyle(fontSize: 44),
          ),
        ],
      ),
    );
  }

  Widget createNonProductPage() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Lo sentimos, pero el producto $productCode no existe...',
          style: const TextStyle(fontSize: 44),
        ),
      ],
    );
  }

  Widget createCircularProgressIndicatorPage() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 200),
      child: Container(
        width: double.infinity,
        height: 400,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey, //New
              blurRadius: 8.0,
              offset: Offset(0, 14),
            ),
          ],
        ),
        child: const Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(),
            Padding(padding: EdgeInsets.all(4)),
            Text('Cargando producto...'),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: FutureBuilder(
        future: getProductInfo(
          productCode,
        ),
        builder: (context, snapshot) {
          Widget widget;

          if (snapshot.hasError) {
            widget = createErrorProductPage();
          } else if (snapshot.hasData) {
            if (snapshot.data!.isEmpty) {
              widget = createNonProductPage();
            } else {
              widget = AppProductDescriptionCard(snapshot.data![0]);
            }
          } else {
            widget = createCircularProgressIndicatorPage();
          }

          return ListView(
            scrollDirection: Axis.vertical,
            children: [
              widget,
              const AppFooter(),
            ],
          );
        },
      ),
    );
  }
}
