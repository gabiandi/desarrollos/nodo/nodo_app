import 'package:flutter/material.dart';
import 'package:nodo_app/src/components/app_shoping_list_products.dart';
import '../components/app_bar.dart';
import '../components/app_footer.dart';

class ShopingListPage extends StatelessWidget {
  const ShopingListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: const SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            AppShopingListProducts(),
            AppFooter(),
          ],
        ),
      ),
    );
  }
}
