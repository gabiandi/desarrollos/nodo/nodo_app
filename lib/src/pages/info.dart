import 'package:flutter/material.dart';
import '../components/app_bar.dart';
import '../components/app_footer.dart';

class InfoPage extends StatelessWidget {
  const InfoPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: const SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            Text('Info'),
            AppFooter(),
          ],
        ),
      ),
    );
  }
}
